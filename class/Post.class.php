<?php
  class Post extends Connection {
    private $table_name = "post";

    private $id;
    private $message;
    public $comments;

    public function __construct(){
      // echo 'A classe "', __CLASS__, '" foi instanciada!<br />';
    }

    public function getId() {
      return $this->id;
    }

    public function setId($id) {
      $this->id = $id;
    }

    public function getMessage() {
      return $this->message;
    }

    public function setMessage($message){
      $this->message = $message;
    }

    public function addComment(Comment $comment) {
      $this->comments[] = $comment;
    }

    public function select() {
      $pdo = parent::getDB();
      $select = $pdo->prepare("SELECT message FROM " . $this->table_name . " WHERE id = ?");
      $select->bindValue(1, $this->id);
      $select->execute();

      if($select->rowCount() == 1):
        $data = $select->fetch(PDO::FETCH_OBJ);
        $this->message = $data->message;
      endif;
    }

    public function create() {
      $pdo = parent::getDB();
      $create = $pdo->prepare("INSERT INTO " . $this->table_name . " (message, user_id) VALUES (?, ?)");
      $create->bindValue(1, $this->message);
      $create->bindValue(2, 1);
      $create->execute();

      return $create->rowCount() == 1 ? true : false;
    }

    public function update() {
      $pdo = parent::getDB();
      $update = $pdo->prepare("UPDATE " . $this->table_name . " SET message = ? WHERE id = ?");
      $update->bindValue(1, $this->message);
      $update->bindValue(2, $this->id);
      $update->execute();

      return $update->rowCount() == 1 ? true : false;
    }

    public function delete() {
      $pdo = parent::getDB();
      $delete = $pdo->prepare("DELETE FROM " . $this->table_name . " WHERE id = ?");
      $delete->bindValue(1, $this->id);
      $delete->execute();

      return $delete->rowCount() == 1 ? true : false;
    }

    public function list() {
      $pdo = parent::getDB();
      $list = $pdo->prepare("SELECT * FROM " . $this->table_name);
      $list->execute();
      
      return $list->fetchAll(PDO::FETCH_OBJ);
    }

    public function __toString(){
      // echo "Usando o método toString: ";
    }

    public function __destruct(){
      // echo 'A classe "', __CLASS__, '" foi destruída.<br />';
    }
  }
?>