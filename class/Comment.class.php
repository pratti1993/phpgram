<?php
  class Comment {
    private $id;
    private $message;
    public $user_id;
    public $post_id;

    public function __construct(){
      echo 'A classe "', __CLASS__, '" foi instanciada!<br />';
    }

    public function getId() {
      return $this->id;
    }

    public function getMessage() {
      return $this->message;
    }

    public function setMessage($message){
      $this->message = $message;
    }

    public function __toString(){
      echo "Usando o método toString: ";
    }

    public function __destruct(){
      echo 'A classe "', __CLASS__, '" foi destruída.<br />';
    }
  }
?>