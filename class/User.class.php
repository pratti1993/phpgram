<?php
  class User extends Connection {
    private $table_name = "user";

    private $id;
    private $first_name;
    private $last_name;
    private $gender;
    private $acceptTerm;
    private $profile_picture_name;
    private $email;
    private $username;
    private $password;
    public $posts;

    public function __construct(){
      // echo 'A classe "', __CLASS__, '" foi instanciada!<br />';
    }

    public function getId() {
      return $this->id;
    }

    public function setId($id) {
      $this->id = $id;
    }

    public function getFirstName() {
      return $this->first_name;
    }

    public function setFirstName($first_name){
      $this->first_name = $first_name;
    }

    public function getLastName() {
      return $this->last_name;
    }

    public function setLastName($last_name){
      $this->last_name = $last_name;
    }

    public function getGender() {
      return $this->gender;
    }

    public function setGender($gender){
      $this->gender = $gender;
    }

    public function getAcceptTerm() {
      return $this->acceptTerm;
    }

    public function setAcceptTerm($accept){
      $this->acceptTerm = $accept;
    }

    public function getProfilePictureName() {
      return $this->profile_picture_name;
    }

    public function setProfilePictureName($name){
      $this->profile_picture_name = $name;
    }

    public function getEmail() {
      return $this->email;
    }

    public function setEmail($email){
      $this->email = $email;
    }

    public function getUsername() {
      return $this->username;
    }

    public function setUsername($username){
      $this->username = $username;
    }

    public function getPassword() {
      return $this->password;
    }

    public function setPassword($password){
      $this->password = $password;
    }

    public function addPost(Post $post) {
      $this->posts[] = $post;
    }

    public function signin(){
      $pdo = parent::getDB();
      $signin = $pdo->prepare("SELECT * FROM " . $this->table_name . " WHERE email = ? AND password = ?");
      $signin->bindValue(1, $this->getUsername());
      $signin->bindValue(2, $this->getPassword());
      $signin->execute();

      if ($signin->rowCount() == 1):
        $data = $signin->fetch(PDO::FETCH_OBJ);
        $_SESSION['user'] = $data->username;
        $_SESSION['logged'] = true;
        echo $_SESSION['user'];
        echo $_SESSION['logged'];
        return true;
      else:
        return false;
      endif;
    }

    public static function signout() {
      if(isset($_SESSION['logged'])):
        unset($_SESSION['logged']);
        session_destroy();
        header("Location: index.php");
      endif;
    }

    public function signup(){
      $pdo = parent::getDB();
      $signup = $pdo->prepare("INSERT INTO " . $this->table_name . " (first_name, last_name, gender, accept_terms, profile_picture_name, email, username, password) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
      $signup->bindValue(1, $this->getFirstName());
      $signup->bindValue(2, $this->getLastName());
      $signup->bindValue(3, $this->getGender());
      $signup->bindValue(4, $this->getAcceptTerm());
      $signup->bindValue(5, $this->getProfilePictureName());
      $signup->bindValue(6, $this->getEmail());
      $signup->bindValue(7, $this->getUsername());
      $signup->bindValue(8, $this->getPassword());
      $signup->execute();

      if ($signup->rowCount() == 1):
        // $data = $signup->fetch(PDO::FETCH_OBJ);
        $_SESSION['user'] = $this->getUsername();
        $_SESSION['first_access'] = true;
        $_SESSION['logged'] = true;
        return true;
      else:
        return false;
      endif;
    }

    public function getProfileImage() {
      $pdo = parent::getDB();
      $getImageName = $pdo->prepare("SELECT profile_picture_name FROM " . $this->table_name . " WHERE username = ?");
      $getImageName->bindValue(1, $this->getUsername());
      $getImageName->execute();

      if ($getImageName->rowCount() == 1):
        $data = $getImageName->fetch(PDO::FETCH_OBJ);
        return 'uploads/' . $data->profile_picture_name;
      else:
        return null;
      endif;
    }

    public function __toString(){
      // echo "Usando o método toString: ";
    }

    public function __destruct(){
      // echo 'A classe "', __CLASS__, '" foi destruída.<br />';
    }
  }
?>