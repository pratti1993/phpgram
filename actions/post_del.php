<?php
  require_once "class/Connection.class.php";
  require_once "class/Post.class.php";

  if (filter_input(INPUT_GET, 'post_id')):
    $id = filter_input(INPUT_GET, "post_id", FILTER_SANITIZE_MAGIC_QUOTES);

    $post = new Post;

    $post->setId($id);
    $post->delete();
  endif;

  header('Location: index.php?page=home');
?>