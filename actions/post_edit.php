<?php
  require_once "class/Connection.class.php";
  require_once "class/Post.class.php";

  if (filter_input(INPUT_GET, 'post_id')):
    $id = filter_input(INPUT_GET, "post_id", FILTER_SANITIZE_MAGIC_QUOTES);

    $post = new Post;

    $post->setId($id);
    $post->select();

    if (filter_input(INPUT_POST, 'update_post')):
      $message = filter_input(INPUT_POST, "message", FILTER_SANITIZE_MAGIC_QUOTES);

      $post->setMessage($message);
      echo $post->update();

      header('Location: index.php?page=home');
    endif;
  endif;
?>
<nav id="nav">
  <div class="container-fluid d-flex align-items-center justify-content-center">
    <a href="?page=home" class="logo">Phpgram</a>
  </div>
</nav>
<main id="posts">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-5">
        <div class="create">
          <form action="<?php $_SERVER['PHP_SELF'] ?>" method="POST">
            <div class="form-group">
              <textarea id="message" name="message" class="form-control" rows="4">
                <?php echo $post->getMessage() ?>
              </textarea>
            </div>
            <button id="update_post" name="update_post" type="submit" class="btn btn-primary btn-block">Atualizar</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</main>