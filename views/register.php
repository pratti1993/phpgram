<?php
  require_once "class/Connection.class.php";
  require_once "class/User.class.php";

  $erro = "";
  $upload_dir = "uploads/";
  
  if (isset($_POST['signup'])):
    $first_name = filter_input(INPUT_POST, "first_name", FILTER_SANITIZE_MAGIC_QUOTES);
    $last_name = filter_input(INPUT_POST, "last_name", FILTER_SANITIZE_MAGIC_QUOTES);
    $gender = filter_input(INPUT_POST, "gender", FILTER_SANITIZE_MAGIC_QUOTES);
    $accept_term = filter_input(INPUT_POST, "accept_term", FILTER_SANITIZE_MAGIC_QUOTES);
    $email = filter_input(INPUT_POST, "email", FILTER_SANITIZE_MAGIC_QUOTES);
		$username = filter_input(INPUT_POST, "username", FILTER_SANITIZE_MAGIC_QUOTES);
    $password = filter_input(INPUT_POST, "password", FILTER_SANITIZE_MAGIC_QUOTES);

    $accepted_upload = 1;
    $image_file_name = basename($_FILES['profile_picture']['name']);
    $image_file_type = strtolower(pathinfo($image_file_name, PATHINFO_EXTENSION));
    $image_file_name = time().uniqid(md5()) . "." . $image_file_type;
    $image_file = $upload_dir . $image_file_name;

    if($image_file_type != "jpg" && $image_file_type != "png" && $image_file_type != "jpeg" && $image_file_type != "gif" ) {
      $erro = "Arquivo invalido!";
      $accepted_upload = 0;
    }

    if ($_FILES['profile_picture']["size"] > 500000) {
      $erro = "Imagem muito grande!";
      $accepted_upload = 0;
    }

    $user = new User;
    $user->setFirstName($first_name);
    $user->setLastName($last_name);
    $user->setGender($gender);
    $user->setAcceptTerm($accept_term);
    $user->setProfilePictureName($image_file_name);
    $user->setEmail($email);
		$user->setUsername($username);
    $user->setPassword($password);

    if($accepted_upload):
      if(move_uploaded_file($_FILES['profile_picture']['tmp_name'], $image_file) && $user->signup()):
        header('Location: index.php?page=home');
      else:
        $erro = "Erro ao cadastrar";
      endif;
    endif;
  endif;
?>
<main id="no-logged">
  <div class="container-fluid">
    <h1 class="logo">Phpgram</h1>
    <div class="content">
      <?php if($erro): ?>
        <div class="alert alert-warning">
          <?php echo $erro ?>
        </div>
      <?php endif; ?>
      <form action="<?php $_SERVER['PHP_SELF'] ?>" method="POST" enctype="multipart/form-data">
        <div class="form-group">
          <input id="first_name" name="first_name" type="text" placeholder="Nome" class="form-control" />
        </div>
        <div class="form-group">
          <input id="last_name" name="last_name" type="text" placeholder="Sobrenome" class="form-control" />
        </div>
        <div class="form-group">
          <input id="email" name="email" type="email" placeholder="E-mail" class="form-control" />
        </div>
        <div class="form-group text-left">
          <label class="d-block">Sexo:</label>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="gender" id="gender1" value="m">
            <label class="form-check-label" for="gender2">Masculino</label>
          </div>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="gender" id="gender1" value="f">
            <label class="form-check-label" for="gender2">Feminino</label>
          </div>
        </div>
        <div class="form-group text-left">
          <label for="profile_picture">Foto do perfil</label>
          <input type="file" id="profile_picture" name="profile_picture" class="form-control-file">
        </div>
        <hr/>
        <div class="form-group">
          <input id="username" name="username" type="text" placeholder="Apelido" class="form-control" />
        </div>
        <div class="form-group">
          <input id="password" name="password" type="password" placeholder="Senha" class="form-control" />
        </div>
        <div class="form-group">
          <input id="repeat_password" name="repeat_password" type="password" placeholder="Repetir senha" class="form-control" />
        </div>
        <div class="form-group">
          <div class="form-check text-left">
            <input type="checkbox" class="form-check-input" name="accept_term" id="accept_term">
            <label class="form-check-label" for="accept_term">Concordo com os termos de uso e política de privacidade</label>
          </div>
        </div>
        <button id="signup" name="signup" type="submit" class="btn btn-primary btn-block">Criar conta</button>
      </form>
    </div>
    <p>Já faz parte? <a href="?page=login">Entre agora mesmo</a></p>
  </div>
</main>