<?php
  require_once "class/Connection.class.php";
  require_once "class/User.class.php";
  $erro = "";
  
  if (isset($_POST['sigin'])):
		$username = filter_input(INPUT_POST, "username", FILTER_SANITIZE_MAGIC_QUOTES);
    $password = filter_input(INPUT_POST, "password", FILTER_SANITIZE_MAGIC_QUOTES);

		$user = new User;
		$user->setUsername($username);
    $user->setPassword($password);
    
		if(!$user->signin()):
      $erro = "Usuário ou senha invalida";
    else:
      header('Location: index.php?page=home');
    endif;
  endif;
?>
<main id="no-logged" class="no-logged-login">
  <div class="container-fluid">
    <div class="wrapper">
    <h1 class="logo">Phpgram</h1>
      <div class="content">
        <?php if($erro): ?>
          <div class="alert alert-warning">
            <?php echo $erro ?>
          </div>
        <?php endif; ?>
        <form action="<?php $_SERVER['PHP_SELF'] ?>" method="POST">
          <div class="form-group">
            <input id="username" name="username" type="text" placeholder="E-mail" class="form-control" />
          </div>
          <div class="form-group">
            <input id="password" name="password" type="password" placeholder="Senha" class="form-control" />
          </div>
          <button id="sigin" name="sigin" type="submit" class="btn btn-primary btn-block">Entrar</button>
        </form>
      </div>
      <p>Ainda não possui uma conta? <a href="?page=register">Faça parte</a></p>
    </div>
  </div>
</main>