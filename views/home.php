<?php
  require_once "class/Connection.class.php";
  require_once "class/User.class.php";
  require_once "class/Post.class.php";

  if(!isset($_SESSION['logged'])):
    header('Location: index.php?page=login');
  endif;

  $user = new User;
  $post = new Post;
  
  $user->setUsername($_SESSION['user']);

  if (isset($_POST['create_post'])):
    $message = filter_input(INPUT_POST, "message", FILTER_SANITIZE_MAGIC_QUOTES);
    $post->setMessage($message);
    echo $post->create();
  endif;

  $posts = $post->list();
?>
<nav id="nav">
  <div class="container d-flex align-items-center justify-content-center">
    <a href="?page=home" class="logo">Phpgram</a>
    <a class="user-signout" href="?action=signout">Sair</a>
  </div>
</nav>
<main id="posts">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-5">
        <div class="create">
          <form action="<?php $_SERVER['PHP_SELF'] ?>" method="POST">
            <div class="form-group">
              <textarea id="message" name="message" class="form-control" rows="4">
              </textarea>
            </div>
            <button id="create_post" name="create_post" type="submit" class="btn btn-primary btn-block">Publicar</button>
          </form>
        </div>
        <?php foreach($posts as $post) { ?>
          <div class="card post">
            <div class="card-header">
              <div class="user-image">
                <img src="<?php echo $user->getProfileImage() ?>" />
              </div>
              <div class="dropdown">
                <a href="#" class="dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">more_horiz</i>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                  <a class="dropdown-item" href="?action=post_edit&post_id=<?php echo $post->id ?>">Editar</a>
                  <a class="dropdown-item" href="?action=post_del&post_id=<?php echo $post->id ?>">Excluir</a>
                </div>
              </div>
            </div>
            <div class="card-body">
              <blockquote class="blockquote mb-0">
                <p><?php echo $post->message ?></p>
                <footer class="blockquote-footer">por <cite title="Source Title"><?php echo $_SESSION['user'] ?></cite></footer>
              </blockquote>
            </div>
          </div>
        <?php } ?>
      </div>
    </div>
  </div>
</main>