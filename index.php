<?php include('includes/head.php') ?>
<?php
  session_start();
  $page = 'home';
  $action = '';
  $folder = "views";

  if(filter_input(INPUT_GET, 'page')) {
    $page = filter_input(INPUT_GET, 'page');
  }

  if(isset($_SESSION['logged']) && filter_input(INPUT_GET, 'action')) {
    $folder = 'actions';
    $page = filter_input(INPUT_GET, 'action');
  }
  
  include('./' . $folder . '/' . $page . '.php');
?>
<?php include('includes/scripts.php') ?>